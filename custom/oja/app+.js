
function __index() {
  var _html = $('#content').html();
  var _content = marked($('#content').html());
  $('#app').html(_content);
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
  $('a[href^="#"]').each(function(i, block) {
    $(this).attr('target', '_blank');
  });
}

__$app = function() {

// ? cheat ? .. I have no intention of running this anyways apart from gsites ..
__root = 'https://google-static-hosting-52080749.bitbucket.io/custom/oja/'; __cache=false;

__require('@/[module]', function(require, module, exports) {

var _Module = function(o) {
  var _this = this;
  this.boot();
};
var prototype = {
  boot: function() {
    var _this = this;
    console.log('Boot!');
  },
  init: function() {
    var _this = this;
    console.log('Init');
    this.make();
  },
  make: function() {
    var _this = this;
    console.log('Make!');
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;

});

};

__scripts = function(options) {
  // vendor
  __script('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/marked/0.4.0/marked.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/markdown.min.js');
  // app
  //__script('app.js');
  //__script('index.js');
};

__styles = function(options) {
  // vendor
  __style('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css');
  __style('https://cdnjs.cloudflare.com/ajax/libs/Primer/10.8.1/build.css');
  // app
  __style('app+.css');
};

__$require=function(r){require=function t(o,f,c){function _(n,r){if(!f[n]){if(!o[n]){var e="function"==typeof require&&require;if(!r&&e)return e(n,!0);if(q)return q(n,!0);var u=new Error("Cannot find module '"+n+"'");throw u.code="MODULE_NOT_FOUND",u}var i=f[n]={exports:{}};o[n][0].call(i.exports,function(r){var e=o[n][1][r];return _(e||r)},i,i.exports,t,o,f,c)}return f[n].exports}for(var q="function"==typeof require&&require,r=0;r<c.length;r++)_(c[r]);return _}(r||{1:[function(r,e,n){},{}]},{},[])},__require=function(r,e){var n={};n[r]=[e,{}],__$require(n)},__$require();

__script=function(r,t){t=t||{};var c="";"http://"!==r.substring(0,7)&&"https://"!==r.substring(0,8)&&(c=t.root||__root||__script.root);var _=t.cache||__cache||__script.cache?"":"?"+Math.random();__script.scripts[r]||document.write('<script src="'+c+r+_+'" onload="__script.onload(\''+c+r+"') \" onerror=\"__script.onerror('"+c+r+"') \"><\/script>")},__script.scripts={},__script.root="",__script.cache=!0,__script.onerror=function(r){__script.scripts[r]=!1},__script.onload=function(r){__script.scripts[r]=!0},__root=null,__cache=null;

__style=function(_,t){t=t||{};var e="";"http://"!==_.substring(0,7)&&"https://"!==_.substring(0,8)&&(e=t.root||__root||__style.root);var l=t.cache||__cache||__style.cache?"":"?"+Math.random();__style.styles[_]||document.write('<link href="'+e+_+l+'" onload="__style.onload(\''+e+_+"') \" onerror=\"__style.onerror('"+e+_+'\') " rel="stylesheet">')},__style.styles={},__style.root="",__style.cache=!0,__style.onerror=function(_){__style.styles[_]=!1},__style.onload=function(_){__style.styles[_]=!0},__link=__style,__root=null,__cache=null;

__$app();
