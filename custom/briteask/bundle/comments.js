


/******************************************************************************/

__scripts = function(options) {
  // vendor
  __script('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/jquery.textcomplete/1.8.0/jquery.textcomplete.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.19/marked.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js');
  // app
  __script('core/backend.js');
  __script('more/jquery-comments.js');
  __script('custom/comments.js');
  __script('index/comments.js');
};

__styles = function(options) {
  // vendor
  __style('https://viima.github.io/jquery-comments/demo/css/jquery-comments.css');
  __style('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
  __style('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css');
  __style('https://cdnjs.cloudflare.com/ajax/libs/Primer/9.6.0/build.css');
  // app
  __style('styles/comments.css');
};

/******************************************************************************/

// 2018-08-18

__$require = function(modules) {
  var _modules = modules || {1:[function(require,module,exports){},{}]};
  require = (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})(_modules,{},[]);
};
__require = function(name, func) {
  var o = {};
  o[name] = [func, {}];
  __$require(o);
};
__$require();

/******************************************************************************/

// 2018-08-18

__script = function(script, options) {
  options = options || {};

  var _root   = '';
  if ((script.substring(0,7) !== 'http://') && (script.substring(0,8) !== 'https://')) {
    _root     = options.root  || (__root  || __script.root);
  }
  var _cache  = (options.cache || (__cache || __script.cache)) ? '' : ('?' + Math.random());

  if (!__script.scripts[script]) {
    document.write(
      '<script src="' + _root + script + _cache +
      '" onload="__script.onload(\'' + _root + script + '\') ' + 
      '" onerror="__script.onerror(\'' + _root + script + '\') ' + 
      '"></' + 'script>'
    );
  }
};
__script.scripts = {};
__script.root    = '';
__script.cache   = true;
__script.onerror = function(script) {
  __script.scripts[script] = false;
};
__script.onload  = function(script) {
  __script.scripts[script] = true;
};
__root = null; __cache = null;

/******************************************************************************/

// 2018-08-18

__style = function(style, options) {
  options = options || {};

  var _root   = '';
  if ((style.substring(0,7) !== 'http://') && (style.substring(0,8) !== 'https://')) {
    _root     = options.root  || (__root  || __style.root);
  }
  var _cache  = (options.cache || (__cache || __style.cache)) ? '' : ('?' + Math.random());

  if (!__style.styles[style]) {
    document.write(
      '<link href="' + _root + style + _cache +
      '" onload="__style.onload(\'' + _root + style + '\') ' + 
      '" onerror="__style.onerror(\'' + _root + style + '\') ' + 
      '" rel="stylesheet">'
    );
  }
};
__style.styles  = {};
__style.root    = '';
__style.cache   = true;
__style.onerror = function(style) {
  __style.styles[style] = false;
};
__style.onload  = function(style) {
  __style.styles[style] = true;
};
__link = __style;
__root = null; __cache = null;

/******************************************************************************/

// 2018-08-18

__c = function(f, o, texts) {
  var text = f.toString()
    .replace(/^[^\/]+\/\*!?/, '')
    .replace(/\*\/[^\/]+$/, '');
  texts    = texts || true;
  
  if (o)     text = __c.supplant(text, o);
  if (texts) __c.text(text);
  
  return text;
};
__c.texts = {};
__c.delimiter = {start: '(@~|', end: '|~@)'};
__c.text = function(text, start, end, texts) {
  start = start || __c.delimiter.start;
  end   = end   || __c.delimiter.end;
  texts = texts || true;
  var index_start   = text.indexOf(start);
  var index_end     = text.indexOf(end);
  if ((index_start>-1) && (index_end>-1)) {
    var name        = text.substring(index_start+start.length, index_end);
    if (texts) __c.texts[name] = text;
    return name;
  }
  return null;
};
__c.supplant = function (text, o) {
  return text.replace(/{([^{}]*)}/g,
    function (a, b) {
      var r = o[b];
      return typeof r === 'string' || typeof r === 'number' ? r : a;
    }
  );
};
__c.t = function(t) {
  var text = t[0];
  
  var f = function(texts) {
    texts = texts !== false ? true : false;
    if (texts) __c.text(text);
    return text;
  }
  f.text = text;
  
  return f;
};

/******************************************************************************/


