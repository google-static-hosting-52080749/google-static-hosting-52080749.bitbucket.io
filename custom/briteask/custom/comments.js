
var Backend = require('@/core/backend');
var backend = new Backend();

function htmlDecode(input) {
  var doc = new DOMParser().parseFromString(input, "text/html");
  return doc.documentElement.textContent;
}
var __render = function() {
	$('.content').each(function(el, i) {
		var _text = htmlDecode($(this).html());
		var _html = marked(_text);
	
		$(this).html(_html);
	});
	$('.content div.code').each(function(i, block) {
		//hljs.highlightBlock(block);
	});
}

$(function() {

	$('#app').comments({
		profilePictureURL: 'https://viima-app.s3.amazonaws.com/media/user_profiles/user-icon.png',
		currentUserId: 1,
		roundProfilePictures: true,
		textareaRows: 5,
		enableAttachments: false,
		enableHashtags: true,
		enablePinging: true,
		//deleteText: 'Remove',
		getUsers: function(success, error) {
			console.log('getUsers');
			setTimeout(function() {
				success(usersArray);
			}, 500);
		},
		getComments: function(success, error) {
			console.log('getComments');
			backend.commentsList(function(err, data) {
				if (!err) {
					// sort
					data = data.sort((a, b) => {
						if (moment(a.date_created).unix() > moment(b.date_created).unix()) {
							return 1;
						}
						if (moment(a.date_created).unix() < moment(b.date_created).unix()) {
							return -1;
						}
						return 0;
					});
					// ?

					var _data= [];
					for (var i=0; i<data.length; i++) {
						var item  = {};
						var _upvoters = JSON.parse(data[i].upvoters);
						item.id       					 = data[i].id;
						item.parent   					 = data[i].parent_id || null; // SO IMPORTANT THIS null
						item.created  					 = moment(data[i].date_created).format('YYYY-MM-DD');
						item.modified 					 = moment(data[i].date_updated).format('YYYY-MM-DD');
                        try {
						  item.content  				 = JSON.parse(data[i].text);
                        } catch(e) {
						  item.content  				 = data[i].text;
                        }
						item.pings    					 = [];
						item.creator  					 = 126;
						item.fullname 					 = 'OJ Abesamis';
						item.profile_picture_url 		 = 'https://app.viima.com/static/media/user_profiles/user-icon.png';
						item.created_by_admin 	 		 = false;
						// ?
						item.created_by_current_user     = data[i].user === __user ? true : false;
						item.upvote_count 			 	 = _upvoters.length;
						item.user_has_upvoted 	 		 = _upvoters.indexOf(__user) > -1 ? true : false;
						// ?
						//item.created_by_current_user   = false;
						//item.upvote_count              = 0;
						//item.user_has_upvoted          = false;
						// ?
						_data.push(item);
					}
					success(_data);

					// ? MARKED, HIGHLIGHT
					__render();
					// ?

				} else {
					error();
				}
			}, {

			}); // IMPORTANT FILTER BY QUESTION
		},
		postComment: function(data, success, error) {
			console.log('postComment', data);
			var item = {};
			var _date = moment().format('YYYY-MM-DD HH:mm:s');

			item.text           = JSON.stringify(data.content);
			item.question_id 	= __params.question;
            item.user           = __user;
			item.parent_id   	= data.parent || null;
			item.upvoters    	= '[]';
			item.date_created = _date;
			item.date_updated = _date;

			backend.commentsCreate(item, function() {
				comments.fetchDataAndRender();
			});
		},
		putComment: function(data, success, error) {
			console.log('putComment', data);
			var id   = data.id;
			var item = {};

			item.text = JSON.stringify(data.content);

			backend.commentsUpdate(id, item, function() {
				comments.fetchDataAndRender();
			});
		},
		deleteComment: function(data, success, error) {
			console.log('deleteComments', data);
			var id   = data.id;

			backend.commentsDestroy(id, function() {
				comments.fetchDataAndRender();
			});
		},
		upvoteComment: function(data, success, error) {
			console.log('upvoteComment', data);
			var id   = data.id;
			backend.commentsUpvote(id, function() {
				comments.fetchDataAndRender(); // SHOULD I ALWAYS REFRESH .. 
			});
		},
	});
});

var usersArray = [
  {
    "id": 1,
    "email": "aamir@britecore.com",
    "fullname": "Aamir Rind",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 2,
    "email": "abbym@britecore.com",
    "fullname": "Abby Monsees",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 3,
    "email": "abdul@britecore.com",
    "fullname": "Abdul Azeez",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 4,
    "email": "abby@britecore.com",
    "fullname": "Abigail Stegeman",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 5,
    "email": "adam.lockwood@britecore.com",
    "fullname": "Adam Lockwood",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 6,
    "email": "adam.travis@britecore.com",
    "fullname": "Adam Travis",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 7,
    "email": "agon.gjonbalaj@britecore.com",
    "fullname": "Agon Gjonbalaj",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 8,
    "email": "ajay@britecore.com",
    "fullname": "Ajay Singh",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 9,
    "email": "albert.hoxha@britecore.com",
    "fullname": "Albert Hoxha",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 10,
    "email": "alexmamone@britecore.com",
    "fullname": "Alex Mamone",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 11,
    "email": "alex.nesterov@britecore.com",
    "fullname": "Alex Nesterov",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 12,
    "email": "alex@britecore.com",
    "fullname": "Alex Rychyk",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 13,
    "email": "amanda@britecore.com",
    "fullname": "Amanda Quint",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 14,
    "email": "amy@britecore.com",
    "fullname": "Amy Putman",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 15,
    "email": "andie@britecore.com",
    "fullname": "Andie Nasby",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 16,
    "email": "drew.tripp@britecore.com",
    "fullname": "Andrew Tripp",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 17,
    "email": "ankur@britecore.com",
    "fullname": "Ankur Dulwani",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 18,
    "email": "anuj@britecore.com",
    "fullname": "Anuj Bansal",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 19,
    "email": "artem@britecore.com",
    "fullname": "Artem Rodin",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 20,
    "email": "ashley@britecore.com",
    "fullname": "Ashley Popham",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 21,
    "email": "atticus@britecore.com",
    "fullname": "Atticus Marker",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 22,
    "email": "augusto@britecore.com",
    "fullname": "Augusto Goulart",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 23,
    "email": "brad@britecore.com",
    "fullname": "Brad Rice",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 24,
    "email": "brandon@britecore.com",
    "fullname": "Brandon Greene",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 25,
    "email": "briteapps@britecore.com",
    "fullname": "BriteApps Admin",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 26,
    "email": "admin@britecore.com",
    "fullname": "BriteCore Admin",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 27,
    "email": "camilo@britecore.com",
    "fullname": "Camilo Diaz",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 28,
    "email": "carlos@britecore.com",
    "fullname": "Carlos Pérez Ceballos",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 29,
    "email": "chastin@britecore.com",
    "fullname": "Chastin Reynolds",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 30,
    "email": "chris@britecore.com",
    "fullname": "Chris Reynolds, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 31,
    "email": "christalent@britecore.com",
    "fullname": "Chris Talent",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 32,
    "email": "cristiano@britecore.com",
    "fullname": "Cristiano Cortezia",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 33,
    "email": "danielhebrink@britecore.com",
    "fullname": "Daniel Hebrink",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 34,
    "email": "danielpareja@britecore.com",
    "fullname": "Daniel Pareja",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 35,
    "email": "daniel@britecore.com",
    "fullname": "Daniel Wilhite, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 36,
    "email": "david@britecore.com",
    "fullname": "David Almendarez",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 37,
    "email": "davidarokiasamy@britecore.com",
    "fullname": "David Arokiasamy",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 38,
    "email": "delisson@britecore.com",
    "fullname": "Délisson Silva",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 39,
    "email": "demian@britecore.com",
    "fullname": "Demian Calcaprina",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 40,
    "email": "dillon@britecore.com",
    "fullname": "Dillon Johnson",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 41,
    "email": "dinesh@britecore.com",
    "fullname": "Dinesh Arora",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 42,
    "email": "dmitriy@britecore.com",
    "fullname": "Dmitriy Velichko",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 43,
    "email": "edward.c@britecore.com",
    "fullname": "Edward C",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 44,
    "email": "ernest@britecore.com",
    "fullname": "Ernest Ten",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 45,
    "email": "ernesto@britecore.com",
    "fullname": "Ernesto Rubio Fuentes",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 46,
    "email": "filipe@britecore.com",
    "fullname": "Filipe Amaral",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 47,
    "email": "francisco@britecore.com",
    "fullname": "Francisco Valdez de la Fuente",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 48,
    "email": "gabe@britecore.com",
    "fullname": "Gabriel De Luca",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 49,
    "email": "geogie@britecore.com",
    "fullname": "Geogie Tom",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 50,
    "email": "gleb@britecore.com",
    "fullname": "Gleb Sevruk",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 51,
    "email": "grant@britecore.com",
    "fullname": "Grant McConnaughey",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 52,
    "email": "hamza@britecore.com",
    "fullname": "Hamza Gul",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 53,
    "email": "heather@britecore.com",
    "fullname": "Heather Putnam",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 54,
    "email": "hugo@britecore.com",
    "fullname": "Hugo Alvarado Arroyo",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 55,
    "email": "igor@britecore.com",
    "fullname": "Igor Kozintsev",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 56,
    "email": "ivan@britecore.com",
    "fullname": "Ivan Rodriguez",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 57,
    "email": "bot@britecore.com",
    "fullname": "IWS Bot",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 58,
    "email": "quoting@britecore.com",
    "fullname": "IWS Quotes",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 59,
    "email": "jakeb@britecore.com",
    "fullname": "Jacob Boomgaarden",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 60,
    "email": "jacob@britecore.com",
    "fullname": "Jacob Foster",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 61,
    "email": "jake@britecore.com",
    "fullname": "Jacob Laird",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 62,
    "email": "jaket@britecore.com",
    "fullname": "Jake Teaford",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 63,
    "email": "jesse@britecore.com",
    "fullname": "James Esse",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 64,
    "email": "jasondurst@britecore.com",
    "fullname": "Jason",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 65,
    "email": "jennifer@britecore.com",
    "fullname": "Jennifer Settlow",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 66,
    "email": "jessica@britecore.com",
    "fullname": "Jessica Sunga",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 67,
    "email": "jonathan@britecore.com",
    "fullname": "Jonathan Carpenter",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 68,
    "email": "jose@britecore.com",
    "fullname": "Jose Santos",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 69,
    "email": "justin@britecore.com",
    "fullname": "Justin Cimino, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 70,
    "email": "katiek@britecore.com",
    "fullname": "Katie Kowalewski",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 71,
    "email": "kevin@britecore.com",
    "fullname": "Kevin Yount, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 72,
    "email": "kujtim@britecore.com",
    "fullname": "Kujtim Hoxha",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 73,
    "email": "kyle@britecore.com",
    "fullname": "Kyle Perik",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 74,
    "email": "lia@britecore.com",
    "fullname": "Lia Bies",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 75,
    "email": "linda.kirk@britecore.com",
    "fullname": "Linda Kirk",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 76,
    "email": "lindsay@britecore.com",
    "fullname": "Lindsay Brooks",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 77,
    "email": "lukasz@britecore.com",
    "fullname": "Lukasz Mikołajczak",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 78,
    "email": "mac@britecore.com",
    "fullname": "Mac McCormick",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 79,
    "email": "mark@britecore.com",
    "fullname": "Mark Hudson",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 80,
    "email": "shaffer@britecore.com",
    "fullname": "Matt Shaffer",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 81,
    "email": "michael@britecore.com",
    "fullname": "Michael Moede",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 82,
    "email": "michaelstowe@britecore.com",
    "fullname": "Michael Stowe",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 83,
    "email": "mohammad@britecore.com",
    "fullname": "Mohammad Albakri",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 84,
    "email": "myles@britecore.com",
    "fullname": "Myles Loffler, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 85,
    "email": "natalie@britecore.com",
    "fullname": "Natalie Pool",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 86,
    "email": "nelson@britecore.com",
    "fullname": "Nelson Amaral",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 87,
    "email": "nick@britecore.com",
    "fullname": "Nick Cash",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 88,
    "email": "olga@britecore.com",
    "fullname": "Olga Lavrova",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 89,
    "email": "oj@britecore.com",
    "fullname": "Omar Abesamis",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 90,
    "email": "patrick@britecore.com",
    "fullname": "Patrick Ford",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 91,
    "email": "petar@britecore.com",
    "fullname": "Petar Zojcevski",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 92,
    "email": "phil@britecore.com",
    "fullname": "Phil Reynolds",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 93,
    "email": "prasant@britecore.com",
    "fullname": "Prasant Jagalprasad",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 94,
    "email": "ina.grozeva@britecore.com",
    "fullname": "Radostina Grozeva",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 95,
    "email": "rafael@britecore.com",
    "fullname": "Rafael Gomes",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 96,
    "email": "rebecca@britecore.com",
    "fullname": "Rebecca Kelly",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 97,
    "email": "renjitha@britecore.com",
    "fullname": "Renjitha Nair",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 98,
    "email": "rita@britecore.com",
    "fullname": "Rita Webb",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 99,
    "email": "rob@britecore.com",
    "fullname": "Rob Ogle, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 100,
    "email": "rohith@britecore.com",
    "fullname": "Rohith Paripeta Raghunath",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 101,
    "email": "rune@britecore.com",
    "fullname": "Rune Docking",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 102,
    "email": "sabin@britecore.com",
    "fullname": "Sabin Sabu",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 103,
    "email": "sajbeer@britecore.com",
    "fullname": "Sajbeer N",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 104,
    "email": "sandra@britecore.com",
    "fullname": "Sandra Davis",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 105,
    "email": "sarah.ridley@britecore.com",
    "fullname": "Sarah Ridley",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 106,
    "email": "schmar.james@britecore.com",
    "fullname": "Schmar James",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 107,
    "email": "scott@britecore.com",
    "fullname": "Scott Blevins",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 108,
    "email": "shend.carkaxhiu@britecore.com",
    "fullname": "Shend Carkaxhiu",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 109,
    "email": "sherri@britecore.com",
    "fullname": "Sherri Marier",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 110,
    "email": "shirin@britecore.com",
    "fullname": "Shirin Kaviani",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 111,
    "email": "sivan@britecore.com",
    "fullname": "Sivan Fraser",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 112,
    "email": "soc@britecore.com",
    "fullname": "SOC Test User",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 113,
    "email": "srigin@britecore.com",
    "fullname": "Srigin Sivan",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 114,
    "email": "sue@britecore.com",
    "fullname": "Susan Creecy",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 115,
    "email": "susan@britecore.com",
    "fullname": "Susan Marker, IWS",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 116,
    "email": "waseem.jan@britecore.com",
    "fullname": "Syed Waseem Jan",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 117,
    "email": "tayler@britecore.com",
    "fullname": "Tayler Atkisson",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 118,
    "email": "thomas@britecore.com",
    "fullname": "Thomas Johns",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 119,
    "email": "tila@britecore.com",
    "fullname": "Tila Wilson",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 120,
    "email": "trijil@britecore.com",
    "fullname": "Trijil Lukose",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 121,
    "email": "vitaliy@britecore.com",
    "fullname": "Vitaliy Pavlenko",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 122,
    "email": "vivek@britecore.com",
    "fullname": "Vivek Thoppil",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 123,
    "email": "vlad@britecore.com",
    "fullname": "Vlad Kovalenko",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 124,
    "email": "will@britecore.com",
    "fullname": "Will Guldin",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 125,
    "email": "winslow@britecore.com",
    "fullname": "Winslow DiBona",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 126,
    "email": "yuriy@britecore.com",
    "fullname": "Yuriy Rafalsky",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
  {
    "id": 127,
    "email": "oj@britecore.com",
    "fullname": "OJ Abesamis",
    "profile_picture_url": "https://app.viima.com/static/media/user_profiles/user-icon.png"
  },
];
