
__require('@/custom/questions/_custom', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  make: function() {
    var _this = this;
    this.makeList();
    this.makeListFilter('title', {plugins: [], maxItems: 1});
    this.makeListFilter('tags');
    this.makeListFilter('requester', {plugins: [], maxItems: 1});
    this.makeFormInputTags('create');
    this.makeFormInputTags('edit');
    this.makeFormInputText('create');
    this.makeFormInputText('edit');
  },
  custom: function() {
    var _this = this;
    // ?
    // handsontable to prevent overflow over filters ..
    $('.ht_clone_top.handsontable').css('display', 'none');
    // make saveable (update without 'refresh' ..)
    $(`#component__modal__edit__save${this.id}`).click(function() {
      var id    = $(`#component__modal__edit__id${_this.id}`).val();
      var attrs = {
        title: $(`#component__modal__edit__title${_this.id}`).val(),
        //text:  JSON.stringify($(`#component__modal__edit__text${_this.id}`).val()),
        text:  JSON.stringify(_this.inputs['edit']['text'].getValue()),
        tags:  _this.inputs['edit']['tags'].getValue(),
      };
      _this.backend.questionsUpdate(id, attrs, function(err, data) {
        if (err) return;
        _this.notify('Saved');
      });
    });
    $(`#component__modal__edit__save${this.id}`).dblclick(function() {
      console.log('asda')
      if (_this.auto['edit']['text'] === false) {
        _this.auto['edit']['text'] = true;
        $(`#component__modal__edit__save${_this.id}`)
        .removeClass('btn-custom-white')
        .addClass('btn-success');
        _this.notify('Set Autosave');
      } else {
        _this.auto['edit']['text'] = false;
        $(`#component__modal__edit__save${_this.id}`)
        .removeClass('btn-success')
        .addClass('btn-custom-white');
        _this.notify('Remove Autosave', {type: 'error'});
      }
    });
    // autosave
    this.inputs['edit']['text'].on('change',function(instance){
      if ((_this.auto['edit']['text'] === true) && (_this.locks['edit']['text'] === false)) {
        _this.locks['edit']['text'] = true;
        setTimeout(function() {
          try {
            var id    = $(`#component__modal__edit__id${_this.id}`).val();
            var attrs = {
              title: $(`#component__modal__edit__title${_this.id}`).val(),
              text:  JSON.stringify(_this.inputs['edit']['text'].getValue()),
              tags:  _this.inputs['edit']['tags'].getValue(),
            };
            _this.backend.questionsUpdate(id, attrs, function(err, data) {
              if (!err) {
                //_this.notify('Saved'); // TOO MUCH?
              }
              _this.locks['edit']['text'] = false;
            });
          } catch(e) {
            console.log(e);
            _this.locks['edit']['text'] = false;
          }
          
          // GUARANTEED TO ALWAYS RELEASE LOCKS .. NOPE
        }, 1000);
      }
      // get value right from instance
      //yourTextarea.value = cMirror.getValue();
      //console.log(2222);
    });
    // ?
  },
  refresh: function() {
    var _this = this;
    this.backend.questionsList(function(err, data) {
      if (err) return;
      _this.notify('Refresh');
      _this.setData(data);
      _this.setList();
      _this.setListFilter('title');
      _this.setListFilter('tags');
      _this.setListFilter('requester');
      // ?
      _this.inputs['create']['text'].refresh();
      _this.inputs['edit']['text'].refresh();
    // ?
    });
  },
  upvote: function(id) {
    var _this = this;
    this.backend.questionsUpvote(id, function(err, attrs) {
      if (err) return;
      _this.refresh();
    });
  },
  notify: function(msg, type) {
    var _this = this;
    $.notify(msg, {autoHideDelay: 1500, className: type || 'success',});
  },
  toggleEdit: function(user) {
    var _this = this;
    // ?
    if (user !== __user) {
      $(`#component__modal__edit__title${this.id}`).prop('disabled', true);
      //this.inputs.edit.tags.disable();
      this.inputs.edit.text.setOption('readOnly', true);
      $(`#component__modal__edit__destroy${this.id}`).prop('disabled', true);
      $(`#component__modal__edit__update${this.id}`).prop('disabled', true);
      $(`#component__modal__edit__save${this.id}`).prop('disabled', true);
    } else {
      $(`#component__modal__edit__title${this.id}`).prop('disabled', false);
      //this.inputs.edit.tags.disable();
      this.inputs.edit.text.setOption('readOnly', false);
      $(`#component__modal__edit__destroy${this.id}`).prop('disabled', false);
      $(`#component__modal__edit__update${this.id}`).prop('disabled', false);
      $(`#component__modal__edit__save${this.id}`).prop('disabled', false);
    }
    // ?
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
