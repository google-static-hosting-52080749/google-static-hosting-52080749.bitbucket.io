
__require('@/custom/questions/_bind', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  bind: function() {
    var _this = this;
    // ?
    // make creatable
    $(`#component__modal__create__action${this.id}`).click(function() {
      var _date =  moment().format('YYYY-MM-DD HH:mm:s');
      var attrs = {
        title: $(`#component__modal__create__title${_this.id}`).val(),
        //text: JSON.stringify($(`#component__modal__create__text${_this.id}`).val()),
        text: JSON.stringify(_this.inputs['create']['text'].getValue()),
        commenting: 'internal',
        requester: __user,
        upvoters: '[]',
        date_created: _date,
        date_updated: _date,
        tags:  _this.inputs['create']['tags'].getValue(),
      };
      _this.backend.questionsCreate(attrs, function(err, data) {
        if (!err) {
          console.log(data);
          $(`#component__modal__create${_this.id}`).modal('toggle');
          _this.refresh();
          //__discuss(data.commenting, data.id);
        }
      });
    });
    // make updatable
    $(`#component__modal__edit__update${this.id}`).click(function() {
      var id    = $(`#component__modal__edit__id${_this.id}`).val();
      var attrs = {
        title: $(`#component__modal__edit__title${_this.id}`).val(),
        //text:  JSON.stringify($(`#component__modal__edit__text${_this.id}`).val()),
        text:  JSON.stringify(_this.inputs['edit']['text'].getValue()),
        tags:  _this.inputs['edit']['tags'].getValue(),
      };
      _this.backend.questionsUpdate(id, attrs, function() {
        $(`#component__modal__edit${_this.id}`).modal('toggle');
        _this.refresh();
      });
    });
    // make saveable (update without 'refresh' ..)
    
      // .. custom
    
    // make destroyable
    $(`#component__modal__edit__destroy${this.id}`).click(function() {
      var id    = $(`#component__modal__edit__id${_this.id}`).val();
      _this.backend.questionsDestroy(id, function() {
        $(`#component__modal__edit${_this.id}`).modal('toggle');
        _this.refresh();
      });
    });
    // make refreshable
    $(`#component__list__refresh__action${this.id}`).click(function() {
      _this.refresh();
    });
    $(`#component__modal__create${this.id}`).on('hidden.bs.modal', function () {
      _this.clearFormInputsCreate();
    })
    $(`#component__modal__edit${this.id}`).on('hidden.bs.modal', function () {
      _this.clearFormInputsEdit();
    })
    // ?
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
