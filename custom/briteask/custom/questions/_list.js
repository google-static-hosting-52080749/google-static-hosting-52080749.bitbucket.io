
__require('@/custom/questions/_list', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  makeList: function() {
    var _this = this;

    var config = {
      data:               [],
      height:             375,
      rowHeaders:         false, 
      colHeaders:         true,
      preventOverflow:    false,
      columnSorting:      true,
      sortIndicator:      true,
      manualColumnResize: true,
      manualRowResize:    true,
      columns:            [
        { data: 'date',    editor: false },
        { data: '_id',     editor: false },
        { data: 'upvotes', editor: false, 
          renderer: function (instance, td, row, col, prop, value, cellProperties) {
            var _id = _this.list.getDataAtCell(row,1);
            //_this.upvote(ids[0]);
            var fragment = document.createDocumentFragment();
            var span = document.createElement('span');
            // color: #2793e6 !important;
            span.innerHTML = `<button class='btn btn-default btn-xs' style='display: block; margin: auto; '
              onclick='app.upvote("${_id}")'>
              ${value}&nbsp;
              <i class="fa fa-thumbs-up" aria-hidden="true"></i>
              </button>`;
            fragment.appendChild(span);
            Handsontable.Dom.empty(td);
            $(td).css('vertical-align', 'middle'); // ?
            td.appendChild(fragment);
            return td;
          } 
        },
        { data: '_title',  editor: false, renderer: 'html'},
        { data: 'tags',
          renderer: function (instance, td, row, col, prop, value, cellProperties) {
            var customTags = function(td, value) {
              var fragment = document.createDocumentFragment();
              if (value) {
                var tags  = value.split('|');
                if (tags) {
                  for (var i=0; i<tags.length; i++) {
                    var span = document.createElement('span');
                    span.className = 'content__tag';
                    span.innerHTML = tags[i];
                    fragment.appendChild(span);
                  }
                }
              } else {
                var span = document.createElement('span');
                span.className = 'content__tag';
                fragment.appendChild(span);
              }
              Handsontable.Dom.empty(td);
              td.appendChild(fragment);
              return td;
            };
            return customTags(td, value);
          }, editor: false
        },
        { data: 'requester', editor: false },
      ],
      columnSorting: {
        column: 0,
        sortOrder: false,
      },
      colHeaders:         [
        'Date', 'ID', 'Upvotes', 'Title', 'Tags', 'Requester'
      ],
      colWidths:          [
        90, 0.1, 70, 485, 275, 165,
      ],
      contextMenu: {
        callback: function(key, options) {
          var getIds = function() {
            var selected = _this.list.getSelectedRange();
            var from     = selected.from.row
            var to       = selected.to.row
            var min      = from < to ? from : to;
            var max      = from > to ? from : to;
            var ids      = [];
            for (var i = min; i <= max; i++) {
              ids.push(_this.list.getDataAtCell(i,1))
            }
            return ids;
          }

          switch (key) {
            case 'view':
              var ids = getIds();
              if (ids.length === 1) {
                var link = _this.list.getDataAtCell(ids[0],4);
                console.log(ids[0]);
                //__discuss(link);
              } else {
                alert('Select only 1 row', 'error');
              }
              break;
            case 'edit':
              var ids = getIds();
              if (ids.length === 1) {
                _this.launchEdit(ids[0]);
              } else {
                alert('Select only 1 row', 'error');
              }
              break;
            case 'upvote':
              var ids = getIds();
              if (ids.length === 1) {
                _this.upvote(ids[0]);
              } else {
                alert('Select only 1 row', 'error');
              }
              break;
            default:
              break;
          }
        },
        items:{
          //'view':  { name: 'View',    },
          //'upvote':  { name: 'Upvote',    },
          //'create':  { name: 'Create',    },
          'edit':    { name: 'Edit',    },
        },
      },
    };
    this.list = new Handsontable(document.getElementById('component__list' + this.id), config);
  },
  makeListFilter: function(column, options) {
    var _this = this;
    options = options || {};
    var _options = {
      valueField: 'value',
      labelField: 'text',
      options: [],
      plugins: options.plugins || ['remove_button'],
      maxItems: options.maxItems || null,
      delimiter: options.delimiter || '|',
      onChange: function(val) {
        _this.filterList();
      },
    };
    this.filters[column] = $(`#component__list__filters__${column}${this.id}`).selectize(_options)[0].selectize;
  },
  filterList: function() {
    var _this = this;
    var filters   = {};
    var data      = this.getData();
    var filtered  = [];
    var all_blank = true;
    for (var filter in this.filters) {
      filters[filter] = this.filters[filter].getValue().split('|');
      if (filters[filter].indexOf('') === -1) {
        all_blank = false
      }
    }
    if (all_blank) {
      filtered = data;
    } else {
      filtered = this.filterData(data, filters); 
    }
    //filtered = this.calcData(filtered);
    this.setList(filtered);
  },
  setListFilter: function(column) {
    var _this = this;
    var filter = this.filters[column];
    var tags   = {};
    var _data = _.union.apply({}, (
      _.chain(this.getData()).map(column).uniq().value()
      .map(function(el){ 
        var _tags = (el + '').split('|');
        for (var i=0; i<_tags.length; i++) {
          if (!tags[_tags[i]]) {
            tags[_tags[i]] = 1;
          } else {
            tags[_tags[i]]++;
          }
        }
        return _tags;
      }))
    );
    var data = [];
    if (column === 'tags') {
      data = _data
        .map(function(el){ return {value: el, text: (el + ' (' + tags[el] + ')'), } });
    } else {
      data = _data
        .map(function(el){ return {value: el, text: el, } });
    }
    // ? sort
    data = data.sort((a, b) => {
      if (a.value > b.value) {
        return 1;
      }
      if (a.value < b.value) {
        return -1;
      }
      return 0;
    });
    // ?
    filter.clearOptions();
    filter.addOption(data);
  },
  setList: function(data) {
    var _this = this;
    data = data || this.getData();
    this.list.updateSettings({ data: data });
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
