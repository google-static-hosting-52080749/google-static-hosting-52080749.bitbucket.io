
__require('@/custom/questions/_render', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  render: function(node) {
    var _this = this;
    node = node || this.node;
    var template = `
      <section class='content-header'>
        <h1>Questions</h1>
      </section>
      <section class='content'>
        <div class='nav-tabs-custom'>

          <ul class='nav nav-tabs'>
            <li class='active'><a href='#content__main__tab${this.id}' data-toggle='tab' aria-expanded='true'>Main</a></li>
          </ul>

          <div class='tab-content'>

            <div id='content__main__tab${this.id}' class='tab-pane fade active in'>
              <div class='row'>

                <div class='col-md-12'>

                  <button data-toggle='modal' data-target='#component__modal__create${this.id}' class='btn btn-success'>
                    <i class='fa fa-plus'></i>
                  </button>

                  &nbsp;&nbsp;
                  
                  <button data-toggle='collapse' data-target='#component__list__filters${this.id}' class='btn btn-default'>
                    <i class='fa fa-search'></i>
                  </button>

                  &nbsp;&nbsp;
                  
                  <button id='component__list__refresh__action${this.id}' class='btn btn-custom-white'>
                    <i class='fa fa-refresh'></i>
                  </button>

                </div>
                
              </div>

              <br>

              <div class='row'>

                <div class='col-md-6'>
                  <span class='component__list__filters'>
                  Title
                  </span>
                  <input id='component__list__filters__title${this.id}' placeholder='Title'>
                </div>

                <div class='col-md-6'>
                  <span class='component__list__filters'>
                  Tags
                  </span>
                  <input id='component__list__filters__tags${this.id}' placeholder='Tags'>
                </div>

                <div class='col-md-12 collapse' id='component__list__filters${this.id}'  aria-expanded='false'>

                  <div class='row'>

                    <div class='col-md-12'>
                      <span class='component__list__filters'>
                      Requester
                      </span>
                      <input id='component__list__filters__requester${this.id}' placeholder='Requester'>
                    </div>

                  </div>

                </div>

              </div>

              <br>

              <div class='row'>

                <div class='col-md-12'>
                  <div class='table-responsive' id='component__list__container${this.id}'>
                    <div id='component__list${this.id}'></div>
                  </div>
                </div>
              </div>


            </div>
            
          </div>

        </div>
      </section>

      <!-- CREATE -->
      <div class='modal fade' id='component__modal__create${this.id}' tabindex='-1' role='dialog'>
        <div class='modal-dialog modal-lg' role='document'>
          <div class='modal-content'>

            <div class='modal-header'>

              <div class='form-group'>
                <div class='col-sm-2'></div>
                <div class='col-sm-10'>
                  <h4 class='modal-title'>Ask a question</h4>
                </div>
              </div>

              <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>

            </div>

            <div class='modal-body'>
            
              <form id='component__modal__create__form${this.id}' data-parsley-validate='' class='form-horizontal' novalidate=''>

                <div class='form-group'>
                  <label for='component__modal__create__title${this.id}' class='col-sm-2 control-label'>Title</label>
                  <div class='col-sm-10'>
                    <input id='component__modal__create__title${this.id}' class='form-control'>
                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__create__text${this.id}' class='col-sm-2 control-label'>Question</label>
                  <div class='col-sm-10'>

                    <div class='nav-tabs-custom'>

                      <ul class='nav nav-tabs'>
                        <li class='active'>
                          <a href='#component__modal__create__text__tab__editor${this.id}' data-toggle='tab' aria-expanded='true'>
                            Editor
                          </a>
                        </li>
                        <li>
                          <a href='#component__modal__create__text__tab__preview${this.id}' data-toggle='tab' aria-expanded='true'>
                            Preview
                          </a>
                        </li>
                      </ul>

                      <div class='tab-content'>
                        
                        <div id='component__modal__create__text__tab__editor${this.id}' class='tab-pane active in'>
                          <textarea id='component__modal__create__text${this.id}' class='form-control' rows='9'></textarea>
                        </div>
                        
                        <div id='component__modal__create__text__tab__preview${this.id}' class='tab-pane '>
                          <iframe id='component__modal__create__text__preview${this.id}' style='border: 0; width: 100%; height: 400px;'></iframe>
                        </div>

                      </div>

                    </div>

                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__create__tags${this.id}' class='col-sm-2 control-label'>Tags</label>
                  <div class='col-sm-10'>
                    <div id='component__modal__create__tags${this.id}'></div>
                  </div>
                </div>

              </form>

            </div>

            <div class='modal-footer'>
              <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
              <button id='component__modal__create__action${this.id}' type='button' class='btn btn-success'>Create</button>
            </div>

          </div>
        </div>
      </div>

      <!-- EDIT/VIEW -->
      <div class='modal fade' id='component__modal__edit${this.id}' tabindex='-1' role='dialog'>
        <div class='modal-dialog modal-lg' role='document'>
          <div class='modal-content'>

            <div class='modal-header'>

              <div class='form-group'>
                <div class='col-sm-2'></div>
                <div class='col-sm-10'>
                  <h4 class='modal-title'>Question <code id='component__modal__edit__id${this.id}'></code></h4>
                </div>
              </div>

              <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>

            </div>

            <div class='modal-body'>
            
              <form id='component__modal__edit__form${this.id}' data-parsley-validate='' class='form-horizontal' novalidate=''>

                <div class='form-group' style='display: none;'>
                  <label for='component__modal__edit__id${this.id}' class='col-sm-2 control-label'>ID</label>
                  <div class='col-sm-10'>
                    <input id='component__modal__edit__id${this.id}' class='form-control' disabled='disabled'>
                  </div>
                </div>

                <div class='form-group' style='display: none;>
                  <label for='component__modal__edit__link${this.id}' class='col-sm-2 control-label'>Link</label>
                  <div class='col-sm-10'>
                    <input id='component__modal__edit__link${this.id}' class='form-control' disabled='disabled'>
                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__edit__title${this.id}' class='col-sm-2 control-label'>Title</label>
                  <div class='col-sm-10'>
                    <input id='component__modal__edit__title${this.id}' class='form-control'>
                  </div>
                </div>

                <div class='form-group'>

                  <label for='component__modal__edit__text${this.id}' class='col-sm-2 control-label'>
                    Question
                    <br>
                    <button 
                      id='component__modal__edit__save${this.id}'
                      class='btn btn-custom-white btn-sm' 
                      style='display: block;margin: auto;float: right;top: 10px;position: relative;'
                      onclick='return false;'
                      >
                      <i class='fa fa-save' aria-hidden='true'></i>
                    </button>
                  </label>

                  <div class='col-sm-10'>

                    <div class='nav-tabs-custom'>

                      <ul class='nav nav-tabs'>
                        <li class='active'>
                          <a href='#component__modal__edit__text__tab__editor${this.id}' data-toggle='tab' aria-expanded='true'>
                            Editor
                          </a>
                        </li>
                        <li>
                          <a href='#component__modal__edit__text__tab__preview${this.id}' data-toggle='tab' aria-expanded='true'>
                            Preview
                          </a>
                        </li>
                      </ul>

                      <div class='tab-content'>
                        
                        <div id='component__modal__edit__text__tab__editor${this.id}' class='tab-pane active in'>
                          <textarea id='component__modal__edit__text${this.id}' class='form-control' rows='9'></textarea>
                        </div>
                        
                        <div id='component__modal__edit__text__tab__preview${this.id}' class='tab-pane '>
                          <iframe id='component__modal__edit__text__preview${this.id}' style='border: 0; width: 100%; height: 400px;'></iframe>
                        </div>

                      </div>

                    </div>

                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__edit__tags${this.id}' class='col-sm-2 control-label'>Tags</label>
                  <div class='col-sm-10'>
                    <div id='component__modal__edit__tags${this.id}'></div>
                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__edit__requester${this.id}' class='col-sm-2 control-label'>Requester</label>
                  <div class='col-sm-10'>
                    <input id='component__modal__edit__requester${this.id}' class='form-control' disabled='disabled'>
                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__edit__upvoters${this.id}' class='col-sm-2 control-label'>
                    Upvoters
                    <br>
                    <button class='btn btn-default btn-xs' style='display: block;margin: auto;float: right;top: 5px;position: relative;'
                      onClick='return false;'
                      >
                      X &nbsp;
                      <i class='fa fa-thumbs-up' aria-hidden='true'></i>
                    </button>
                  </label>
                  <div class='col-sm-10'>
                    <textarea id='component__modal__edit__upvoters${this.id}' class='form-control' disabled='disabled'></textarea>
                  </div>
                </div>

                <div class='form-group'>
                  <label for='component__modal__edit__comments${this.id}' class='col-sm-2 control-label'>Comments<br><br></label>
                  <div class='col-sm-10'></div>
                  <div class='col-sm-12'>
                    <iframe id='component__modal__edit__comments${this.id}' style='border: 0; width: 100%; height: 500px; '></iframe>
                  </div>
                </div>

              </form>

            </div>

            <div class='modal-footer'>
              <button id='component__modal__edit__destroy${this.id}' type='button' class='btn btn-default pull-left'>Delete</button>
              <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
              <button id='component__modal__edit__update${this.id}' type='button' class='btn btn-success'>Update </button>
            </div>

          </div>
        </div>

        <!-- VIEW -->
      </div>

    `;
    $(node).html(template);
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
