
__require('@/custom/questions/_form', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  makeFormInputTags: function(form) {
    var _this = this;
    var options = {
      labelField: 'value',
      options: [],
      plugins: ['remove_button', 'drag_drop'],
      persist: false,
      delimiter: '|',
      onChange: function(val) {},
      create: function(val) {
        return { 'value': val, 'text': val };
      },
    };
    this.inputs[form]['tags'] = $(`#component__modal__${form}__tags${this.id}`).selectize(options)[0].selectize;
  },
  makeFormInputText: function(form) {
    var _this = this;

    this.inputs[form]['text'] = CodeMirror.fromTextArea(
      document.getElementById(`component__modal__${form}__text${this.id}`), {
        lineNumbers:  true,
        autoRefresh:  true,
        lineWrapping: true,
        mode: {
          name: 'gfm',
          tokenTypeOverrides: {
            //emoji: "emoji"
          }
        },
      }
    );
    this.inputs[form]['text'].refresh();
    this.inputs[form]['text'].setSize(null, 400);
  },
  clearFormInputsCreate: function() {
    var _this = this;
    $(`#component__modal__create__title${_this.id}`).val('');
    //$(`#component__modal__create__text${_this.id}`).val('');
    _this.inputs['create']['text'].setValue(Array(18).join('\n'));
    setTimeout(function() { _this.inputs['create']['text'].refresh() }, 700);
    _this.inputs['create']['tags'].setValue('');
  },
  clearFormInputsEdit: function() {
    var _this = this;
    $(`#component__modal__edit__id${_this.id}`).val('');
    $(`#component__modal__edit__link${_this.id}`).val('');
    $(`#component__modal__edit__title${_this.id}`).val('');
    //$(`#component__modal__edit__text${_this.id}`).val('');
    _this.inputs['edit']['text'].setValue('');
    setTimeout(function() { _this.inputs['edit']['text'].refresh() }, 700);
    _this.inputs['edit']['tags'].setValue('');
    $(`#component__modal__edit__requester${_this.id}`).val('');
    $(`#component__modal__edit__upvoters${_this.id}`).val('');
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
