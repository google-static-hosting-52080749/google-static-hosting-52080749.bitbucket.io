
__require('@/custom/questions/_launch', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  launchCreate: function() {
    var _this = this;
  },
  launchEdit: function(_id) {
    var _this = this;
    var items = this.data.filter(function(el) {
      return (el['_id'] === _id);
    });
    var id = items[0].id;
    this.backend.questionsFind(id, function(err, attrs) {
      if (err) return;
      // ?
      _this.toggleEdit(attrs.requester);
      //_this.clearFormInputsEdit();
      $(`#component__modal__edit__id${_this.id}`).html(id);
      // ?
      $(`#component__modal__edit${_this.id}`).modal('toggle');
      // ?
      $(`#component__modal__edit__id${_this.id}`).val(id);
      $(`#component__modal__edit__link${_this.id}`).val(attrs.link || '');
      $(`#component__modal__edit__title${_this.id}`).val(attrs.title || '');
      try {
        //$(`#component__modal__edit__text${_this.id}`).val(JSON.parse(attrs.text) || '');
        _this.inputs['edit']['text'].setValue(JSON.parse(attrs.text) || '');
        setTimeout(function() { _this.inputs['edit']['text'].refresh() }, 300);
      } catch(e) {
        console.log(e);
        //$(`#component__modal__edit__text${_this.id}`).val(attrs.text || '');
        _this.inputs['edit']['text'].setValue(attrs.text);
        setTimeout(function() { _this.inputs['edit']['text'].refresh() }, 300);
      }
      //$(`#component__modal__edit__tags${_this.id}`).val(attrs.tags || '');
      _this.inputs['edit']['tags'].clearOptions();
      if (attrs.tags) {
        var _val    = attrs.tags.split('|');
        var options = _val.map(function(el,i){return {value:el, text:el}});
        _this.inputs['edit']['tags'].addOption(options);
        _this.inputs['edit']['tags'].setValue(_val);
      }
      // ?
      $(`#component__modal__edit__requester${_this.id}`).val(attrs.requester || '');
      $(`#component__modal__edit__upvoters${_this.id}`).val(attrs.upvoters || '');
      // __id is global ..
      //var _iframe = `https://script.google.com/a/britecore.com/macros/s/${__id}/exec?index=comments_static&question=${id}`;
      //$(`#component__modal__edit__comments${_this.id}`).attr('src', _iframe);
    });
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
