
__require('@/custom/questions/_data', function(require, module, exports) {

var _Module = function() {
  var _this = this;
};
var prototype = {
  filterData: function(data, filters) {
    var _this = this;
    var filtered = [];
    var _filters = {};
    for (var filter in filters) {
      if (!(filters[filter].length === 1 && filters[filter][0] === '')) {
        _filters[filter] = filters[filter]
      }
    }

    for (var i=0; i<data.length; i++) {
      var inside = true;
      for (var filter in _filters) {
        var _data = (data[i][filter] + '').split('|');
        // ?
        if (_.difference(filters[filter], _data).length === 0) {
          inside = true;
        } else {
          inside = false;
          break;
        }
        // ?
        /*
        for (var j=0; j<_data.length; j++) {

          if ((filters[filter].indexOf(_data[j] + '') === -1)
            || (_data[j] === '')) {
            inside = false;
          } else {
            inside = true;
            break;
          }
        }
        */
      }
      if (inside) filtered.push(data[i]);
    }
    return filtered;
  },
  setData: function(data) {
    var _this = this;
    this.data = JSON.parse(JSON.stringify(data));
  },
  getData: function() {
    var _this = this;
    var data = JSON.parse(JSON.stringify(this.data));
    return data;
  },
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;
  
});
