
__require('@/custom/questions', function(require, module, exports) {

var Backend = require('@/core/backend');
var _Render = require('@/custom/questions/_render');
var _Custom = require('@/custom/questions/_custom');
var _Bind   = require('@/custom/questions/_bind');
var _Data   = require('@/custom/questions/_data');
var _List   = require('@/custom/questions/_list');
var _Form   = require('@/custom/questions/_form');
var _Launch = require('@/custom/questions/_launch');

var _Module = function(node) {
  var _this = this;
  this.id      = (Math.random()+'').substr(2,8);
  this.node    = node || document.getElementById('component');
  this.backend = new Backend({ });
  this.data    = [];
  this.list    = {};
  this.filters = {};
  this.auto    = { edit:{ text: false, }, };
  this.locks   = { edit:{ text: false, }, };
  this.inputs  = { create:{}, edit:{}, };
  this.boot();
};
var prototype = {
  boot: function() {
    var _this = this;
  },
  init: function() {
    var _this = this;
    this.render();
    this.make();
    this.bind();
    this.refresh();
    this.custom(); //
  },
  // _custom
  make: function() {
    var _this = this;
  },
  custom: function() {
    var _this = this;
  },
  refresh: function() {
    var _this = this;
  },
  notify: function(msg, type) {
    var _this = this;
  },
  toggleEdit: function(user) {
    var _this = this;
  },
  upvote: function(id) {
    var _this = this;
  },
  // _render
  render: function(node) {
    var _this = this;
  },
  // _bind
  bind: function() {
    var _this = this;
  },
  // _list
  makeList: function() {
    var _this = this;x
  },
  makeListFilter: function(column, options) {
    var _this = this;
  },
  filterList: function() {
    var _this = this;
  },
  setListFilter: function(column) {
    var _this = this;
  },
  setList: function(data) {
    var _this = this;
  },
  // _form
  makeFormInputTags: function(form) {
    var _this = this;
  },
  makeFormInputText: function(form) {
    var _this = this;
  },
  clearFormInputsCreate: function() {
    var _this = this;
  },
  clearFormInputsEdit: function() {
    var _this = this;
  },
  // _data
  filterData: function(data, filters) {
    var _this = this;
  },
  setData: function(data) {
    var _this = this;
  },
  getData: function() {
    var _this = this;
  },
  // _launch
  launchCreate: function() {
    var _this = this;
  },
  launchEdit: function(id) {
    var _this = this;
  },
  _action: function(param) {
    var _this = this;
    return param;
  },
};
for (var i in prototype)         _Module.prototype[i] = prototype[i];
for (var i in _Render.prototype) _Module.prototype[i] = _Render.prototype[i];
for (var i in _Custom.prototype) _Module.prototype[i] = _Custom.prototype[i];
for (var i in _Bind.prototype)   _Module.prototype[i] = _Bind.prototype[i];
for (var i in _Data.prototype)   _Module.prototype[i] = _Data.prototype[i];
for (var i in _List.prototype)   _Module.prototype[i] = _List.prototype[i];
for (var i in _Form.prototype)   _Module.prototype[i] = _Form.prototype[i];
for (var i in _Launch.prototype) _Module.prototype[i] = _Launch.prototype[i];

module.exports = _Module;

});
