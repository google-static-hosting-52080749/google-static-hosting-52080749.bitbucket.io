
__require('@/custom/questions', function(require, module, exports) {

var Backend = require('@/core/backend');
var _Core   = require('@/custom/questions/_core');
var _List   = require('@/custom/questions/_list');
var _Form   = require('@/custom/questions/_form');

var _Module = function(o) {
  var _this = this;
  this.node    = o.node || document.getElementById('component');
  this.id      = (Math.random()+'').substr(2,8);
  this.backend = new Backend();
  this.data    = [];
  this.list    = {};
  this.filters = {};
  this.auto    = { edit:{ text: false, }, };
  this.locks   = { edit:{ text: false, }, };
  this.inputs  = { create:{}, edit:{}, };
  this.boot();
};
var prototype = {
  boot: function() {
    var _this = this;
  },
  init: function() {
    var _this = this;
    this.render();
    this.refresh();
  },
  // _core
  render:               function(node) { }, // ? if no node, return string
  _renderLayout:        function() { }, 
  _renderList:          function() { },
  _renderForm:          function() { },
  _renderFormCreate:    function() { },
  _renderFormEdit:      function() { },
  //bind:                 function() { },
  refresh:              function() { },
  // _list
  listRenderBind:       function(node) { },
  listRenderBindFilter: function(node) { },
  listRefresh:          function() { },
  listBind:             function() { },
  listFilter:           function() { },
  listSetData:          function() { },
  listSetFilterData:    function() { },
  listFilterData:       function() { },
  // _form
  formRenderBind:       function(node) { },
  formRenderInput:      function(node) { },
  formRenderInputTags:  function(form) { },
  formRenderInputText:  function(form) { },
  formRefresh:          function() { },
  formBind:             function() { },
  formToggleEdit:       function() { },
  // data
  dataSet:              function(data) { },
  dataGet:              function() { },
  dataFilter:           function(data, filters) { },
  // special
  notify:               function() { },
  // 
  _action: function() {
    var _this = this;
  },
};
for (var i in prototype)         _Module.prototype[i] = prototype[i];
for (var i in _Core.prototype)   _Module.prototype[i] = _Core.prototype[i];
for (var i in _List.prototype)   _Module.prototype[i] = _List.prototype[i];
for (var i in _Form.prototype)   _Module.prototype[i] = _Form.prototype[i];

module.exports = _Module;

});
