
__require('@/core/backend', function(require, module, exports) {

var _Module = function(o) {
  var _this = this;
  this._app = o.app || __app;
};
var prototype = {
  init: function() {
    var _this = this;
    this.action();
  },
  questionsFind: function(id, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'findQuestions', id);
  },
  questionsList: function(cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') {
        var data = resp.data;
        for (var i=0; i<data.length; i++) {
          data[i]['_id']     = data[i]['id'].substring(0,9);
          //data[i]['_title'] = `<a onclick="__discuss('${data[i]['commenting']}', '${data[i]['id']}')">${data[i]['title']}</a>`;
          data[i]['_title'] = `<a onclick="app.launchEdit('${data[i]['id']}')">${data[i]['title']}</a>`;
          data[i]['date']   = moment(data[i].date_created).format('YYYY-MM-DD'); // 'YYYY-MM-DD hh:mm'
          try {
            data[i]['upvotes'] = JSON.parse(data[i]['upvoters']).length;
          } catch(e) {
            data[i]['upvotes'] = 0;
          }
        }
        cb(false, data);
      }
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'listQuestions', {});
  },
  questionsCreate: function(attrs, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'createQuestions', attrs);
  },
  questionsUpdate: function(id, attrs, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'updateQuestions', id, attrs);
  },
  questionsDestroy: function(id, cb) {
    var _this = this;
    google.script.run.withSuccessHandler((resp) => {
      cb(false, resp);
    }).app(this._app,'@/core/backend', 'destroyQuestions', id);
  },
  questionsUpvote: function(id, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).__gateway__custom__questions('upvote', { id: id, });
  },
  // ?
  commentsFind: function(id, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'find', {id: id});
  },
  commentsList: function(cb, options) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'list', {table: 'comments', filters:{ question_id: __params.question,  }, });
  },
  commentsCreate: function(attrs, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'create', {table: 'comments', attrs: attrs});
  },
  commentsUpdate: function(id, attrs, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'update', {table: 'comments', id: id, attrs: attrs});
  },
  commentsDestroy: function(id, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).app(this._app,'@/core/backend', 'destroy', {table: 'comments', id: id});
  },
  commentsUpvote: function(id, cb) {
    var _this = this;
    cb = cb || function() { console.log('No callback!') };
    google.script.run.withSuccessHandler((resp) => {
      if (resp.status === 'success') cb(false, resp.data);
      else cb(true, resp.message || '');
    }).__gateway__custom__comments('upvote', { id: id, });
  },
  // ?
  action: function(param) {
    var _this = this;
    return param;
  },
};
for (var i in prototype) _Module.prototype[i] = prototype[i];

module.exports = _Module;

});
