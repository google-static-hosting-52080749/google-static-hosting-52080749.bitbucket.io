
function __index() {
  var _html = $('#content').html();
  var _content = marked($('#content').html());
  $('#app').html(_content);
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
  $('a').each(function(i, block) {
    $(this).attr('target', '_blank');
  });
}
