


__scripts = function() {
  // vendor
  __script('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/marked/0.4.0/marked.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js');
  __script('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/markdown.min.js');
  // app
  __script.cache=false; __script.root = 'https://google-static-hosting-52080749.bitbucket.io/';
  //__script('more/oja/more/attempt.js');
  //__script('more/oja/more/some.js');
  __script('more/oja/index.js');
}

__styles = function() {
  // vendor
  __style('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css');
  // app
  __style.cache=false; __style.root = 'https://google-static-hosting-52080749.bitbucket.io/';
  __style('more/oja/styles.css');
}
