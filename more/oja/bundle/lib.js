
var __script = function(script, options) {
  options = options || {};
  var _root = options.root || __script.root;
  var _cache = __script.cache ? '' : ('?' + Math.random());
  if (!__script.scripts[script]) {
    document.write(
      '<script src="' + _root + script + _cache +
      '" onload="__script.onload(\'' + _root + script + '\') ' + 
      '" onerror="__script.onerror(\'' + _root + script + '\') ' + 
      '"></' + 'script>'
    );
  }
};
__script.scripts = {};
__script.root = '';
__script.cache = true;
__script.onerror = function(script) {
  __script.scripts[script] = false;
};
__script.onload = function(script) {
  __script.scripts[script] = true;
};

var __style = function(style, options) {
  options = options || {};
  var _root = options.root || __style.root;
  var _cache = __style.cache ? '' : ('?' + Math.random());
  if (!__style.styles[style]) {
    document.write(
      '<link href="' + _root + style + _cache +
      '" onload="__style.onload(\'' + _root + style + '\') ' + 
      '" onerror="__style.onerror(\'' + _root + style + '\') ' + 
      '" rel="stylesheet">'
    );
  }
};
__style.styles = {};
__style.root = '';
__style.cache = true;
__style.onerror = function(style) {
  __style.styles[style] = false;
};
__style.onload = function(style) {
  __style.styles[style] = true;
};
__link = __style;
